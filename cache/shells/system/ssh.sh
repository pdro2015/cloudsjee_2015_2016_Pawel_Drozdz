#!/bin/bash

sudo mv /home/vagrant/system/id_rsa /home/vagrant/.ssh/id_rsa
sudo mv /home/vagrant/system/id_rsa.pub /home/vagrant/.ssh/id_rsa.pub

sudo chown vagrant:vagrant /home/vagrant/.ssh/id_rsa
sudo chown vagrant:vagrant /home/vagrant/.ssh/id_rsa.pub

cat /home/vagrant/.ssh/id_rsa.pub >> /home/vagrant/.ssh/authorized_keys

sudo cp -r /home/vagrant/.ssh /root/

sudo chmod 600 /home/vagrant/.ssh/id_rsa
sudo chmod 644 /home/vagrant/.ssh/id_rsa.pub
sudo chmod 600 /root/.ssh/id_rsa
sudo chmod 644 /root/.ssh/id_rsa.pub

sudo apt-get install unzip

cd /home/vagrant/

unzip apache-ignite-fabric-1.6.0-bin.zip

cd apache-ignite-fabric-1.6.0-bin/bin/

sudo chmod 777 ignite.sh

sudo sh ignite.sh

