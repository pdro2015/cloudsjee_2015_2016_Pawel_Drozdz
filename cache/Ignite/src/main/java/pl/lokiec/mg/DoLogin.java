/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lokiec.mg;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Lokiec
 */
@WebServlet(name = "DoLogin", urlPatterns = {"/dologin"})
public class DoLogin extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String user = request.getParameter("login");
        String pass = request.getParameter("pass");
        

        if (user.equals("pawel") && pass.equals("pawel")) {
            HttpSession session = request.getSession();
            session.setAttribute("user", user);
            session.setMaxInactiveInterval(0);
            String encodedURL = response.encodeRedirectURL("login");
            response.sendRedirect(encodedURL);

        } else {
            PrintWriter out = response.getWriter();
            out.println("<font color=red>Zly login lub haslo!!!</font>");
        }
    }
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
