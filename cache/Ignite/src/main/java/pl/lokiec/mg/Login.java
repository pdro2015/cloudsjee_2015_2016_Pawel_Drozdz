/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lokiec.mg;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Lokiec
 */
public class Login extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String begin = "<!DOCTYPE html>\n<html>\n<head>\n<title>Login</title>\n</head>\n<body>";
        try (PrintWriter out = response.getWriter()) {
            out.println(begin);
            String form = "<form method=\"POST\" action=\"dologin\">"
                    + "<input type=\"text\" name=\"login\" placeholder=\"Login\"/> "
                    + "<input type=\"password\" name=\"pass\" placeholder=\"Haslo\"/>"
                    + "<input type=\"submit\" value=\"Zaloguj\"/></form>";
            HttpSession session = request.getSession(false);
            if (session != null && session.getAttribute("user") != null && session.getAttribute("user").equals("pawel")) {
                form = "Jestes zalogowany!<br>"
                + "<form method=\"GET\" action=\"logout\"><input type=\"submit\" value=\"Wyloguj\"/></form>";
            }
            out.println(form);
            out.println("</body>\n</html>");
        }
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
